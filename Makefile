##########################################################
# universal Makefile for any project
# Feel free to use and share it
#
# Creator HoP (hovhannespalyan@openamilbox.org)
##########################################################

SHELL := /bin/bash
COMPILER ?= g++
COMPILERFLAGS += -Wall -Werror -Wextra -Weffc++ -std=c++11
# lib options for compiler
LIBS += -lasound
#LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_stitching
#LIBS += -L/usr/local/lib -lopencv_stitching -lopencv_superres -lopencv_videostab -lopencv_aruco -lopencv_bgsegm -lopencv_bioinspired -lopencv_ccalib -lopencv_dpm -lopencv_face -lopencv_photo -lopencv_freetype -lopencv_fuzzy -lopencv_hdf -lopencv_img_hash -lopencv_line_descriptor -lopencv_optflow -lopencv_reg -lopencv_rgbd -lopencv_saliency -lopencv_stereo -lopencv_structured_light -lopencv_phase_unwrapping -lopencv_surface_matching -lopencv_tracking -lopencv_datasets -lopencv_text -lopencv_dnn -lopencv_plot -lopencv_xfeatures2d -lopencv_shape -lopencv_video -lopencv_ml -lopencv_ximgproc -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_flann -lopencv_xobjdetect -lopencv_imgcodecs -lopencv_objdetect -lopencv_xphoto -lopencv_imgproc -lopencv_core
# source and objcet directorys
SRC_DIR ?= src
OBJ_DIR ?= objs
OBJ_DIR_D := $(addsuffix -d, $(OBJ_DIR))
# definitions
# name of executable (program)
execable ?= setSolver
execable_d := $(addsuffix -d,$(abspath $(execable)))
# autodetect projects in SRC_DIR
PROJECT_DIRS := $(sort $(dir $(abspath $(wildcard $(SRC_DIR)/*/*.hpp))))
# includes
#INCLUDES := $(addprefix -I,$(PROJECT_DIRS))
INCLUDES ?= $(addprefix -I,$(abspath $(SRC_DIR)))
# autodetect *.cpp files
CPPS := $(wildcard $(SRC_DIR)/*/*.cpp) $(wildcard $(SRC_DIR)/main.cpp)
ifdef SKIP
    CPPS := $(filter-out $(wildcard $(SKIP)), $(CPPS))
endif
# all object files
OBJS := $(patsubst $(SRC_DIR)/%,$(OBJ_DIR)/%,$(CPPS:.cpp=.o))
OBJS_D := $(patsubst $(SRC_DIR)/%,$(OBJ_DIR_D)/%,$(CPPS:.cpp=.o))
# all dependensy files
DEPS := $(OBJS:.o=.d)
DEPS_D := $(OBJS_D:.o=.d)
# setting colored output
INTERACTIVE := $(shell [ -t 0 ] && echo 1)
NOCOLORS := 0
NOCOLORS := $(shell tput colors 2> /dev/null)
ifeq ($(shell test $(NOCOLORS) -ge 8 2> /dev/null; echo $$?), 0)
    BOLD := $(shell tput bold)
    RCOLOR := $(shell tput sgr0)
    BLACK := $(shell tput setaf 0)
    RED := $(shell tput setaf 1)
    GREEN := $(shell tput setaf 2)
    YELLOW := $(shell tput setaf 3)
    BLUE := $(shell tput setaf 4)
    MAGENTA := $(shell tput setaf 5)
    CYAN := $(shell tput setaf 6)
    WHITE := $(shell tput setaf 7)
endif

# exporting variables
export SRC_DIR
export OBJ_DIR
export execable
export COMPILERFLAGS
export INCLUDES
export SKIP
export COMPILER
#########################################

_default: _makeODir $(execable)
	@echo -e "$(GREEN)Compiled.$(RCOLOR)"

.PHONY: install debug noassert mingw clean resolve resource_converter clang

install:
	@$(MAKE) --no-print-directory $(MAKEFILE)

debug:
	$(eval COMPILERFLAGS += -g)
	$(eval OBJ_DIR := $(addsuffix -d, $(OBJ_DIR)))
	$(eval execable := $(addsuffix -d, $(execable)))
	@echo -e "$(YELLOW)Debug.$(RCOLOR)"

noassert:
	$(eval DEF_FLAGS += -DNDEBUG)
	$(eval DEF_FLAGS += -DM_NO_DEBUG)
	@echo -e "$(GREEN)Noassert.$(RCOLOR)"

_makeODir:
	@mkdir -p $(OBJ_DIR)

resolve:
	@echo -e "$(GREEN)Resolved.$(RCOLOR)"
	@find $(OBJ_DIR) -name *.d | xargs rm -rf

$(OBJ_DIR)/%.d: $(SRC_DIR)/%.cpp
	@mkdir -p $(dir $@)
	@echo -en "$(YELLOW)Calculating dependencies...$(RCOLOR)\r"
	@$(COMPILER) $(INCLUDES) $(COMPILERFLAGS) $(DEF_FLAGS) -MM $< -o $@
	@sed -i 's|$(notdir $*).o:|$(@:.d=.o):|g' $@

-include $(DEPS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@mkdir -p $(dir $@)
	@echo -e "$(BOLD)$(YELLOW)$(COMPILER) $(COMPILERFLAGS) $(DEF_FLAGS) -c $< -o $@$(RCOLOR)"
	@$(COMPILER) $(INCLUDES) $(COMPILERFLAGS) $(DEF_FLAGS) -c $< -o $@

$(execable): $(OBJS)
	@echo -e "$(BOLD)$(GREEN)$(COMPILER) $(COMPILERFLAGS) $(DEF_FLAGS) <obj_files> -o $@ $(LIBS)$(RCOLOR)"
	@$(COMPILER) $(INCLUDES) $(COMPILERFLAGS) $(LINKERFLAGS) $(DEF_FLAGS) $(OBJS) -o $@ $(LIBS)

clean:
	@rm -rf $(OBJ_DIR)
	@rm -rf $(execable)
	@echo -e "$(GREEN)Cleaned.$(RCOLOR)"

help info:
	@echo -e "\nMakefile to compile $(BOLD)$(GREEN)$(execable)$(RCOLOR)\n"
	@echo -e "------$(RED) Use the following targets $(RCOLOR)-----------------"
	@echo -e "$(MAGENTA)<None>$(RCOLOR) | $(CYAN)install$(RCOLOR)\n\tto make the $(BOLD)$(GREEN)$(execable)$(RCOLOR)."
	@echo -e "$(CYAN)debug$(RCOLOR)\n\tto setup debugging env (use with $(CYAN)install$(RCOLOR) to have a result)."
	@echo -e "$(CYAN)noassert$(RCOLOR)\n\tto add $(YELLOW)-DNDEBUG$(RCOLOR) flag (use with $(CYAN)install$(RCOLOR) to have a result)."
	@echo -e "$(CYAN)clean$(RCOLOR)\n\tto cleanup (use after $(CYAN)debug$(RCOLOR) to cleanup debug build)."
	@echo -e "$(CYAN)resolve$(RCOLOR)\n\tto resolve dependencies after hierarchical changes."
	@echo -e "$(CYAN)help$(RCOLOR) | $(CYAN)info$(RCOLOR)\n\tto type this message."
	@echo -e "------$(RED) Setable variables $(RCOLOR)-------------------------"
	@echo -e "$(YELLOW)COMPILER$(RCOLOR)\n\tto set the compiler."
	@echo -e "\tby default: '$(MAGENTA)$(COMPILER)$(RCOLOR)'"
	@echo -e "$(YELLOW)COMPILERFLAGS$(RCOLOR)\n\tto set compilation flags."
	@echo -e "\tby default: '$(MAGENTA)$(COMPILERFLAGS)$(RCOLOR)'"
	@echo -e "$(YELLOW)SKIP$(RCOLOR)\n\tlist of skipping files(can be used windcard)."
	@echo -e "\tby default: '$(MAGENTA)$(SKIP)$(RCOLOR)'"
	@echo -e "$(YELLOW)LIBS$(RCOLOR)\n\tto set other libs."
	@echo -e "\tby default: '$(MAGENTA)$(LIBS)$(RCOLOR)'"
	@echo -e "$(YELLOW)INCLUDES$(RCOLOR)\n\tto set additional include dirs."
	@echo -e "\tby default: '$(MAGENTA)$(INCLUDES)$(RCOLOR)'"
	@echo -e "$(YELLOW)SKIP$(RCOLOR)\n\tto skip cpp files."
	@echo -e "\tby default: '$(MAGENTA)$(SKIP)$(RCOLOR)'"
	@echo -e "$(YELLOW)execable$(RCOLOR)\n\tto rename the executable (program)."
	@echo -e "\tby default: '$(GREEN)$(execable)$(RCOLOR)'"
	@echo -e "--------------------------------------------------\n"
	@echo -e "For more information please look into Makefile.\n"
