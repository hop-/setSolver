#ifndef CORE_CARD_HPP
#define CORE_CARD_HPP

namespace Core
{

class Card
{
public:
    enum class Type {
        One = 0
        , Two
        , Three
    };

public:
    Card(Type color, Type shape, Type fill, Type count);
    ~card() = default;

public:
    Type color() const;
    Type shape() const;
    Type fill() const;
    Type count() const;

private:
    const Type _color;
    const Type _shape;
    const Type _fill;
    const Type _count;
};

} // namespace Core

#endif // CORE_CARD_HPP
