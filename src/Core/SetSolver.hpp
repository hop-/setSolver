#ifndef CORE_SETSOLVER_HPP
#define CORE_SETSOLVER_HPP

#include "Card.hpp"

#include <array>
#include <vector>

namespace Core
{

class SetSolver
{
public:
    using Cards = std::array<Card, 12>;
    using Set = std::array<unsigned, 3>;
public:
    SetSolver() = delete;
    ~SetSolver() = default;

public:
    static std::vector<Set> solve(Cards cards);
};

} // namespace Core

#endif // CORE_SETSOLVER_HPP
