#include "Card.hpp"

namespace Core
{

Card::
Card(Type color, Type shape, Type fill, Type count)
    : _color(color)
    , _shape(shape)
    , _fill(fill)
    , _count(count)
{
}

Type Card::
color() const
{
    return _color;
}

Type Card::
shape() const
{
    return _shape;
}

Type Card::
fill() const
{
    return _fill;
}

Type Card::
count() const
{
    return _count;
}

} // namespace Core
